<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //change name of the database
    protected $table = "posts";
    //primary key
    protected $primaryKey ="id";
    //Time Stamps
    public  $timestamps=true;
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //
    public function index()
    {
        $title = "#home Page";
      //  return view('aa_Pages.index',compact('title'));
      //this view other link
      return view('aa_Pages.index')->with('title',$title);
    }
    
    
    public function about()
    {
        return view('aa_Pages.about');
    }

    
    public function services()
    {
        $data= array(

            'title'=>'Services#',
            'Services' => ['Web Design','Programming','SEO']
        );
        return view('aa_Pages.services')->with($data);
    }
    
}

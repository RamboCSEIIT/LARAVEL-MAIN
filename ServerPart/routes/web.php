<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
   // return view('welcome');
    return "hello world";
});
Route::get('/about', function () {
    return view('aa_Pages.about');
    
});
//Dynamic route id
Route::get('/users/{id}/{name}', function ($id,$name) {
    return  'This is user id'.$id. 'is from' . $name;
    
}); 
 
  
 */ 

Route::get('/','PageController@index');
Route::get('/about','PageController@about');
Route::get('/services','PageController@services');
Route::resource('blog', 'blog\PostsController');
